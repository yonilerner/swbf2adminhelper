**This project has been moved to: https://github.com/jweigelt/swbf2-adminhelper**

A small tool to help administrating Star Wars Battlefront 2 dedicated servers

Forked from: http://sourceforge.net/projects/swbf2adminhelper/

Trello Board: https://trello.com/b/e8xeQIPg/swbf2-admin-helper